package cat.itb.exercici1;

public class Feina extends Thread{
    private String nomFeina;
    private double euros;

    public Feina(String nomFeina, double euros) {
        this.nomFeina = nomFeina;
        this.euros = euros;
    }

    public String getNomFeina() {
        return nomFeina;
    }

    public void setNomFeina(String nomFeina) {
        this.nomFeina = nomFeina;
    }

    public double getEuros() {
        return euros;
    }

    public void setEuros(double euros) {
        this.euros = euros;
    }

    @Override
    public String toString() {
        return "Feina{" +
                "nomFeina='" + nomFeina + '\'' +
                ", euros=" + euros +
                '}';
    }

    @Override
    public void run() {
        System.out.println(nomFeina +": "+ euros);
    }
}
